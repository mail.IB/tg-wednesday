import configparser

from telegram.ext import Updater, CommandHandler, Dispatcher

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger

from src import storage, handlers
from src.logger import logger


updater: Updater
scheduler: BackgroundScheduler


def send_wednesday():
    for chat_id in storage.get_list():
        updater.bot.send_message(chat_id, 'It is Wednesday my dudes https://youtu.be/Oct2xKMGOno')


def setup_scheduler():
    global scheduler
    scheduler = BackgroundScheduler()
    scheduler.start()
    scheduler.add_job(send_wednesday, CronTrigger(day_of_week='wed', hour=9, minute=0))


def setup_handlers(dp: Dispatcher):
    dp.add_handler(CommandHandler('start', handlers.start))
    dp.add_handler(CommandHandler('stop', handlers.stop))
    dp.add_error_handler(handlers.error)


def main():
    global updater

    config = configparser.ConfigParser()
    config.read('config.ini')
    token: str = config['main']['token']

    storage.init_chat_list()

    setup_scheduler()

    updater = Updater(token)
    dp: Dispatcher = updater.dispatcher
    setup_handlers(dp)

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
