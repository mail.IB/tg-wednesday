from typing import Set


active_chats: Set[int] = set()


def init_chat_list():
    pass


def add_chat(chat_id: int) -> bool:
    if chat_id in active_chats:
        return False

    active_chats.add(chat_id)
    return True


def remove_chat(chat_id: int) -> bool:
    if chat_id not in active_chats:
        return False

    active_chats.remove(chat_id)
    return True


def get_list() -> Set[int]:
    return active_chats
