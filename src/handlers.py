from telegram import Update, Bot, Chat, Message

from src import storage
from src.logger import logger


def start(bot: Bot, update: Update):
    chat: Chat = update.effective_chat
    msg: Message = update.message

    if storage.add_chat(chat.id):
        msg.reply_text('Chat was added to list')
    else:
        msg.reply_text('Current chat is already in the list')


def stop(bot: Bot, update: Update):
    chat: Chat = update.effective_chat
    msg: Message = update.message

    if storage.remove_chat(chat.id):
        msg.reply_text('Chat was removed from list')
    else:
        msg.reply_text('Current chat is not in the list')


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)
